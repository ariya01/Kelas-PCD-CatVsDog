import cv2
import numpy as np
import os
from random import shuffle
from tqdm import tqdm
import tensorflow as tf
import matplotlib.pyplot as plt
import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import input_data, dropout, fully_connected
from tflearn.layers.estimator import regression
MODEL_NAME = 'dogs-vs-cats-convnet'
LR = 1e-3
IMG_SIZE = 50
TEST_DIR = os.getcwd()+"\\test"

tf.reset_default_graph()
convnet = input_data(shape=[None, IMG_SIZE, IMG_SIZE, 1], name='input')
convnet = conv_2d(convnet, 32, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)
convnet = conv_2d(convnet, 64, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)
convnet = conv_2d(convnet, 128, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)
convnet = conv_2d(convnet, 64, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)
convnet = conv_2d(convnet, 32, 5, activation='relu')
convnet = max_pool_2d(convnet, 5)
convnet = fully_connected(convnet, 1024, activation='relu')
convnet = dropout(convnet, 0.8)
convnet = fully_connected(convnet, 2, activation='softmax')
convnet = regression(convnet, optimizer='adam', learning_rate=LR, loss='categorical_crossentropy', name='targets')
model = tflearn.DNN(convnet, tensorboard_dir='log', tensorboard_verbose=0)

if os.path.exists('{}.meta'.format(MODEL_NAME)):
    model.load(MODEL_NAME)
    print('Model Loaded !')

# test_data = np.load('test_data.npy')
img ='2.jpg'
path = os.path.join(TEST_DIR, img)
img_num = img.split('.')[0]
asli = cv2.imread(path)
img_data = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
img_data = cv2.resize(img_data, (IMG_SIZE, IMG_SIZE))
gambar = np.array(img_data)
gambar = gambar.reshape(IMG_SIZE,IMG_SIZE,1)
model_out = model.predict([gambar])
model_out

plt.imshow(asli)
if np.argmax(model_out) == 1:
    plt.title ("Dog")
else:
    plt.title('Cat')
plt.show()